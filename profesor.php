<?php
//Variables
$profesorId=(isset($_POST['profesorId']))?$_POST['profesorId']:"";
$profesorNombre=(isset($_POST['profesorNombre']))?$_POST['profesorNombre']:"";
$profesorApellido=(isset($_POST['profesorApellido']))?$_POST['profesorApellido']:"";
$profesorDni=(isset($_POST['profesorDni']))?$_POST['profesorDni']:"";
$profesorMail=(isset($_POST['profesorMail']))?$_POST['profesorMail']:"";
$profesorMateria=(isset($_POST['profesorMateria']))?$_POST['profesorMateria']:"";
//$conn = conectar();

//echo "conectado";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$accionAgregar="";
$accionModificar=$accionEliminar=$accionCancelar="disable";
$mostrarModal=false;

//Include conexion
include("Conexion/conexion.php");

//CRUD
switch($accion){
    case "btnAgregar":
        echo "agregar";

        $stmt=$conn->prepare('INSERT INTO profesor (nombre, apellido, dni, mail, id_materia) VALUES (?, ?, ?, ?, ?) ');
        $stmt->bind_param("ssiss", $profesorNombre, $profesorApellido, $profesorDni, $profesorMail, $profesorMateria);

        $stmt->execute();

        header("Location: profesor.php");

    break;
    case "btnModificar":
        echo "modificar";

        $stmt=$conn->prepare('UPDATE profesor SET nombre = ?, apellido = ?, dni = ?, mail = ?, id_materia = ? WHERE id = ?');
        $stmt->bind_param("ssiss", $profesorNombre, $profesorApellido, $profesorDni, $profesorMail, $profesorMateria, $profesorId);

        $stmt->execute();

        header("Location: profesor.php");
    break;
    case "btnEliminar":
        echo "eliminar";

        $stmt=$conn->prepare('DELETE FROM profesor WHERE id = ?');
        $stmt->bind_param("s", $profesorId);

        $stmt->execute();

        header("Location: profesor.php");

    break;
    case "btnCancelar":
        header("Location: profesor.php");
        echo "cancelar";
    break;
    case "Seleccionar":

        $accionAgregar="disable";

        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;
        
        echo "Seleccionar";
    break;
    case "salir":
        header("Location: index.php");
        echo "cancelar";
    break;

}

    $stmt="SELECT * FROM profesor";
    $resultado=$conn->query($stmt);
    $listProfesores=$resultado->fetch_all(MYSQLI_ASSOC);

    //print_r($listprofesors);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desarrollo Web</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><h1>Profesores</h1></li>
    </ol>
    </nav>
    <div class="container">
    
        <form action="" method="post" ectype="multipart/form-data">

        <!-- Modal para hacer el abm-->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">profesor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                        <input type="hidden" name="profesorId" placeholder="" id="profesorId" value="<?php echo $profesorId?>">
                        <br>

                        <label for="">Nombre:</label>
                        <input type="text" class="form-control" name="profesorNombre" placeholder="" id="profesorNombre" value="<?php echo $profesorNombre?>">
                        <br>
                        <label for="">Apellido:</label>
                        <input type="text" class="form-control" name="profesorApellido" placeholder="" id="profesorApellido" value="<?php echo $profesorApellido?>">
                        <br>
                        <label for="">DNI:</label>
                        <input type="text" class="form-control" name="profesorDni" placeholder="" id="profesorDni" value="<?php echo $profesorDni?>">
                        <br>
                        <label for="">MAil:</label>
                        <input type="text" class="form-control" name="profesorMail" placeholder="" id="profesorMail" value="<?php echo $profesorMail?>">
                        <br>
                        <label for="">Materia:</label>
                        <input type="text" class="form-control" name="profesorMateria" placeholder="" id="profesorMateria" value="<?php echo $profesorMateria?>">
                        <br>
                </div>
            </div>  
            <div class="modal-footer">

                <button value="btnAgregar" <?php echo $accionAgregar; ?> class="btn btn-success" type="submit" name="accion">Agregar</button>
                <button value="btnModificar" <?php echo $accionModificar; ?> class="btn btn-warning" type="submit" name="accion">Modificar</button>
                <button value="btnEliminar" <?php echo $accionEliminar; ?> class="btn btn-danger" type="submit" name="accion">Eliminar</button>
                <button value="btnCancelar" <?php echo $accionCancelar; ?> class="btn btn-primary" type="submit" name="accion">Cancelar</button>
                
            </div>
            </div>
        </div>
        </div>
        <br>
        <br>
        
        </form>

        <!-- Tabla con todos los datos -->
        <div class="row"> 
            <table class="table table-bordered">
                <thead class="thead-dark ">
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>DNI</th>
                        <th>Mail</th>
                        <th>Materia</th>
                        <th>Acciones</th>
                    </tr>
                </thead>

                <?php foreach($listProfesores as $profesor) {?>
                    <tr>
                        <td scope="row"><?php echo $profesor['id']; ?></td>
                        <td><?php echo $profesor['nombre']; ?></td>
                        <td><?php echo $profesor['apellido']; ?></td>
                        <td><?php echo $profesor['dni']; ?></td>
                        <td><?php echo $profesor['mail']; ?></td>
                        <td><?php echo $profesor['id_materia']; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="profesorId" value="<?php echo $profesor['id']; ?>">
                            <input type="hidden" name="profesorNombre" value="<?php echo $profesor['nombre']; ?>">
                            <input type="hidden" name="profesorApellido" value="<?php echo $profesor['apellido']; ?>">
                            <input type="hidden" name="profesorDni" value="<?php echo $profesor['dni']; ?>">
                            <input type="hidden" name="profesorMail" value="<?php echo $profesor['mail']; ?>">
                            <input type="hidden" name="profesorMateria" value="<?php echo $profesor['id_materia']; ?>">
                        
                        
                            <button value="Seleccionar" class="btn btn-info" type="submit"  name="accion">Seleccionar</button>
                            <button value="btnEliminar" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
                        </form>
                        </td>
                    </tr>   
                <?php }?>
            </table>
    
        </div>

        <!-- Boton agregar registro y Salir -->
        <div>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
            Agregar registro
        </button>
        <input type="button" 
        class="btn btn-primary" 
        name="salir" 
        placeholder="" 
        id="salir" 
        value="salir"
        onclick="window.location='index.php';">
        </div>            
        <?php if($mostrarModal){?>
        <script>
            $("#exampleModal").modal('show');
        </script>
        <?php }?>
    </div>
</body>
</html>