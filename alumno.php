<?php
//Variables
$alumnoId=(isset($_POST['alumnoId']))?$_POST['alumnoId']:"";
$alumnoNombre=(isset($_POST['alumnoNombre']))?$_POST['alumnoNombre']:"";
$alumnoApellido=(isset($_POST['alumnoApellido']))?$_POST['alumnoApellido']:"";
$alumnoDni=(isset($_POST['alumnoDni']))?$_POST['alumnoDni']:"";
$alumnoMail=(isset($_POST['alumnoMail']))?$_POST['alumnoMail']:"";
//$conn = conectar();

//echo "conectado";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$accionAgregar="";
$accionModificar=$accionEliminar=$accionCancelar="disable";
$mostrarModal=false;

//Include conexion
include("Conexion/conexion.php");

//CRUD
switch($accion){
    case "btnAgregar":
        echo "agregar";

        $stmt=$conn->prepare('INSERT INTO alumno (nombre, apellido, dni, mail) VALUES (?, ?, ?, ?) ');
        $stmt->bind_param("ssis", $alumnoNombre, $alumnoApellido, $alumnoDni, $alumnoMail);

        $stmt->execute();

        header("Location: alumno.php");

    break;
    case "btnModificar":
        echo "modificar";

        $stmt=$conn->prepare('UPDATE alumno SET nombre = ?, apellido = ?, dni = ?, mail = ? WHERE id = ?');
        $stmt->bind_param("ssiss", $alumnoNombre, $alumnoApellido, $alumnoDni, $alumnoMail, $alumnoId);

        $stmt->execute();

        header("Location: alumno.php");
    break;
    case "btnEliminar":
        echo "eliminar";

        $stmt=$conn->prepare('DELETE FROM alumno WHERE id = ?');
        $stmt->bind_param("s", $alumnoId);

        $stmt->execute();

        header("Location: alumno.php");

    break;
    case "btnCancelar":
        header("Location: alumno.php");
        echo "cancelar";
    break;
    case "Seleccionar":

        $accionAgregar="disable";

        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;
        
        echo "Seleccionar";
    break;
    case "salir":
        header("Location: alumno.php");
        echo "cancelar";
    break;

}

    $stmt="SELECT * FROM alumno";
    $resultado=$conn->query($stmt);
    $listAlumnos=$resultado->fetch_all(MYSQLI_ASSOC);

    //print_r($listAlumnos);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desarrollo Web</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><h1>ALUMNOS</h1></li>
    </ol>
    </nav>
    <div class="container">
    
        <form action="" method="post" ectype="multipart/form-data">

        <!-- Modal para hacer el abm-->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Alumno</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                        <input type="hidden" name="alumnoId" placeholder="" id="alumnoId" value="<?php echo $alumnoId?>">
                        <br>

                        <label for="">Nombre:</label>
                        <input type="text" class="form-control" name="alumnoNombre" placeholder="" id="alumnoNombre" value="<?php echo $alumnoNombre?>">
                        <br>
                        <label for="">Apellido:</label>
                        <input type="text" class="form-control" name="alumnoApellido" placeholder="" id="alumnoApellido" value="<?php echo $alumnoApellido?>">
                        <br>
                        <label for="">DNI:</label>
                        <input type="text" class="form-control" name="alumnoDni" placeholder="" id="alumnoDni" value="<?php echo $alumnoDni?>">
                        <br>
                        <label for="">Mail:</label>
                        <input type="text" class="form-control" name="alumnoMail" placeholder="" id="alumnoMail" value="<?php echo $alumnoMail?>">
                        <br>
                </div>
            </div>  
            <div class="modal-footer">

                <button value="btnAgregar" <?php echo $accionAgregar; ?> class="btn btn-success" type="submit" name="accion">Agregar</button>
                <button value="btnModificar" <?php echo $accionModificar; ?> class="btn btn-warning" type="submit" name="accion">Modificar</button>
                <button value="btnEliminar" <?php echo $accionEliminar; ?> class="btn btn-danger" type="submit" name="accion">Eliminar</button>
                <button value="btnCancelar" <?php echo $accionCancelar; ?> class="btn btn-primary" type="submit" name="accion">Cancelar</button>
                
            </div>
            </div>
        </div>
        </div>
        <br>
        <br>
        
        </form>

        <!-- Tabla con todos los datos -->
        <div class="row"> 
            <table class="table table-bordered">
                <thead class="thead-dark ">
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>DNI</th>
                        <th>Mail</th>
                        <th>Acciones</th>
                    </tr>
                </thead>

                <?php foreach($listAlumnos as $alumno) {?>
                    <tr>
                        <td scope="row"><?php echo $alumno['id']; ?></td>
                        <td><?php echo $alumno['nombre']; ?></td>
                        <td><?php echo $alumno['apellido']; ?></td>
                        <td><?php echo $alumno['dni']; ?></td>
                        <td><?php echo $alumno['mail']; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="alumnoId" value="<?php echo $alumno['id']; ?>">
                            <input type="hidden" name="alumnoNombre" value="<?php echo $alumno['nombre']; ?>">
                            <input type="hidden" name="alumnoApellido" value="<?php echo $alumno['apellido']; ?>">
                            <input type="hidden" name="alumnoDni" value="<?php echo $alumno['dni']; ?>">
                            <input type="hidden" name="alumnoMail" value="<?php echo $alumno['mail']; ?>">
                        
                        
                            <button value="Seleccionar" class="btn btn-info" type="submit"  name="accion">Seleccionar</button>
                            <button value="btnEliminar" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
                        </form>
                        </td>
                    </tr>   
                <?php }?>
            </table>
    
        </div>

        <!-- Boton agregar registro y Salir -->
        <div>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
            Agregar registro
        </button>
       
        <input type="button" 
        class="btn btn-primary" 
        name="salir" 
        placeholder="" 
        id="salir" 
        value="salir"
        onclick="window.location='index.php';">
        </div>            
        <?php if($mostrarModal){?>
        <script>
            $("#exampleModal").modal('show');
        </script>
        <?php }?>
    </div>
</body>
</html>