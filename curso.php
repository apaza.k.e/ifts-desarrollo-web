<?php
//Variables
$cursoId=(isset($_POST['cursoId']))?$_POST['cursoId']:"";
$cursoNombre=(isset($_POST['cursoNombre']))?$_POST['cursoNombre']:"";
$cursoDia=(isset($_POST['cursoDia']))?$_POST['cursoDia']:"";
$cursoTurno=(isset($_POST['cursoTurno']))?$_POST['cursoTurno']:"";
$cursoIdProfesor=(isset($_POST['cursoIdProfesor']))?$_POST['cursoIdProfesor']:"";
//$conn = conectar();

//echo "conectado";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$accionAgregar="";
$accionModificar=$accionEliminar=$accionCancelar="disable";
$mostrarModal=false;

//Include conexion
include("Conexion/conexion.php");

//CRUD
switch($accion){
    case "btnAgregar":
        echo "agregar";

        $stmt=$conn->prepare('INSERT INTO curso (nombre, dia, turno, id_profesor) VALUES (?, ?, ?, ?) ');
        $stmt->bind_param("ssis", $cursoNombre, $cursoDia, $cursoTurno, $cursoIdProfesor);

        $stmt->execute();

        header("Location: curso.php");

    break;
    case "btnModificar":
        echo "modificar";

        $stmt=$conn->prepare('UPDATE curso SET nombre = ?, dia = ?, turno = ?, id_profesor = ? WHERE id = ?');
        $stmt->bind_param("ssiss", $cursoNombre, $cursoDia, $cursoTurno, $cursoIdProfesor, $cursoId);

        $stmt->execute();

        header("Location: curso.php");
    break;
    case "btnEliminar":
        echo "eliminar";

        $stmt=$conn->prepare('DELETE FROM curso WHERE id = ?');
        $stmt->bind_param("s", $cursoId);

        $stmt->execute();

        header("Location: curso.php");

    break;
    case "btnCancelar":
        header("Location: curso.php");
        echo "cancelar";
    break;
    case "Seleccionar":

        $accionAgregar="disable";

        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;
        
        echo "Seleccionar";
    break;
    case "salir":
        header("Location: index.php");
        echo "cancelar";
    break;

}

    $stmt="SELECT * FROM curso";
    $resultado=$conn->query($stmt);
    $listCursos=$resultado->fetch_all(MYSQLI_ASSOC);

    //print_r($listcursos);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desarrollo Web</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page"><h1>Cursos</h1></li>
    </ol>
    </nav>
    <div class="container">
    
        <form action="" method="post" ectype="multipart/form-data">

        <!-- Modal para hacer el abm-->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                        <input type="hidden" name="cursoId" placeholder="" id="cursoId" value="<?php echo $cursoId?>">
                        <br>

                        <label for="">Nombre:</label>
                        <input type="text" class="form-control" name="cursoNombre" placeholder="" id="cursoNombre" value="<?php echo $cursoNombre?>">
                        <br>
                        <label for="">Dia:</label>
                        <input type="text" class="form-control" name="cursoDia" placeholder="" id="cursoDia" value="<?php echo $cursoDia?>">
                        <br>
                        <label for="">Turno:</label>
                        <input type="text" class="form-control" name="cursoTurno" placeholder="" id="cursoTurno" value="<?php echo $cursoTurno?>">
                        <br>
                        <label for="">Profesor:</label>
                        <input type="text" class="form-control" name="cursoIdProfesor" placeholder="" id="cursoIdProfesor" value="<?php echo $cursoIdProfesor?>">
                        <br>
                </div>
            </div>  
            <div class="modal-footer">

                <button value="btnAgregar" <?php echo $accionAgregar; ?> class="btn btn-success" type="submit" name="accion">Agregar</button>
                <button value="btnModificar" <?php echo $accionModificar; ?> class="btn btn-warning" type="submit" name="accion">Modificar</button>
                <button value="btnEliminar" <?php echo $accionEliminar; ?> class="btn btn-danger" type="submit" name="accion">Eliminar</button>
                <button value="btnCancelar" <?php echo $accionCancelar; ?> class="btn btn-primary" type="submit" name="accion">Cancelar</button>
                
            </div>
            </div>
        </div>
        </div>
        <br>
        <br>
        
        </form>

        <!-- Tabla con todos los datos -->
        <div class="row"> 
            <table class="table table-bordered">
                <thead class="thead-dark ">
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Dia</th>
                        <th>Turno</th>
                        <th>Profesor</th>
                        <th>Acciones</th>
                    </tr>
                </thead>

                <?php foreach($listCursos as $curso) {?>
                    <tr>
                        <td scope="row"><?php echo $curso['id']; ?></td>
                        <td><?php echo $curso['nombre']; ?></td>
                        <td><?php echo $curso['dia']; ?></td>
                        <td><?php echo $curso['turno']; ?></td>
                        <td><?php echo $curso['id_profesor']; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="cursoId" value="<?php echo $curso['id']; ?>">
                            <input type="hidden" name="cursoNombre" value="<?php echo $curso['nombre']; ?>">
                            <input type="hidden" name="cursoDia" value="<?php echo $curso['dia']; ?>">
                            <input type="hidden" name="cursoTurno" value="<?php echo $curso['turno']; ?>">
                            <input type="hidden" name="cursoIdProfesor" value="<?php echo $curso['id_profesor']; ?>">
                        
                        
                            <button value="Seleccionar" class="btn btn-info" type="submit"  name="accion">Seleccionar</button>
                            <button value="btnEliminar" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
                        </form>
                        </td>
                    </tr>   
                <?php }?>
            </table>
    
        </div>

        <!-- Boton agregar registro y Salir -->
        <div>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
            Agregar registro
        </button>
        <input type="button" 
        class="btn btn-primary" 
        name="salir" 
        placeholder="" 
        id="salir" 
        value="salir"
        onclick="window.location='index.php';">
        </div>            
        <?php if($mostrarModal){?>
        <script>
            $("#exampleModal").modal('show');
        </script>
        <?php }?>
    </div>
</body>
</html>